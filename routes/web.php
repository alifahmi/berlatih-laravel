<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@signup');

Route::get('/data-tables', 'IndexController@showdata');


// Route::group(['middleware' => ['auth']], function ()
// {
    // CRUD Cast
    // Create
    Route::get('/cast/create', 'CastController@create'); // route ke form create
    Route::post('/cast', 'CastController@store'); // route utk simpan data ke DB

    // Read
    Route::get('/cast', 'CastController@index'); // route list/daftar cast
    Route::get('/cast/{cast_id}', 'CastController@show'); // route detail cast

    // Update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route menuju ke form edit
    Route::put('/cast/{cast_id}', 'CastController@update'); // route utk update data berdasarkan DB  

    // Delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); // Route untuk hapus data
// });

// CRUD Genre
Route::get('/genre/create', 'GenreController@create');
Route::post('/genre', 'GenreController@store');

Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre_id}', 'GenreController@show');

Route::get('/genre/{genre_id}/edit', 'GenreController@edit'); 
Route::put('/genre/{genre_id}', 'GenreController@update');

Route::delete('/genre/{genre_id}', 'GenreController@destroy');


// CRUD Film
Route::resource('film', 'FilmController');

Route::group(['middleware' => ['auth']], function ()
{
    // Update profile
    Route::resource('profile', 'ProfileController')->only(['index', 'update']);

    // Peran
    Route::resource('peran', 'PeranController')->only(['index', 'store']);
});


Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
