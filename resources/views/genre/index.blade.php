@extends('layout.master')

@section('judul')
    Daftar Genre
@endsection

@section('content')

@auth
  <a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>
@endauth

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Genre</th>
        <th scope="col">List Film</th>
        @auth
        <th scope="col">Action</th>
        @endauth
      </tr>
    </thead>
    <tbody>
     @forelse ($genre as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>
              <ul>
                @foreach ($item->film as $value)
                  <li>{{$value->judul}}</li>                    
                @endforeach
              </ul>
            </td>
            <td>              
              <form action="/genre/{{$item->id}}" method="POST">
                <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                @auth
                <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                @endauth
              </form>               
            </td>
        </tr>
     @empty
     <h1>Data tidak ditemukan</h1>
         
     @endforelse
    </tbody>
  </table>

@endsection