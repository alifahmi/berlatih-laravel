@extends('layout.master')

@section('judul')
    Detail Genre
@endsection

@section('content')

<h1>{{$genre->nama}}</h1>

<div class="row">
    @foreach ($genre->film as $item)
        <div class="col-4">
            <div class="card">
                <img class="card-img-top" src="{{asset('img/' . $item->poster)}}" alt="">
                <div class="card-body">
                    <h5 class="card-title">{{$item->judul}}</h5>
                    <p class="card-text">{{$item->ringkasan}}</p>
                </div>
            </div>
        </div>
    @endforeach
</div>
    
@endsection