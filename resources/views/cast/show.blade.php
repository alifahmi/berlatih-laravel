@extends('layout.master')

@section('judul')
    Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>Umur: {{$cast->umur}} tahun</p>
<p>Bio: {{$cast->bio}}</p>

@endsection