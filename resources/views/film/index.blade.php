@extends('layout.master')

@section('judul')
    Daftar Film
@endsection

@section('content')

@auth
  <a href="/film/create" class="btn btn-primary my-3">Tambah Film</a>
@endauth

<div class="row">
  @forelse ($film as $item)
    <div class="col-4">
      <div class="card">
        <img class="card-img-top" src="{{asset('img/' . $item->poster)}}" alt="{{$item->judul}}">
        <div class="card-body">
          
          <h5 class="card-title">{{$item->judul}}</h5>
          <p class="card-text">{{$item->ringkasan}}</p>
          <form action="/film/{{$item->id}}"  method="POST">
            @csrf
            @method('DELETE')
            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            @auth
            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>  
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            @endauth
          </form>
        </div>
      </div>
    </div>
  
  @empty
  <h4>Belum ada data Film</h4>
      
  @endforelse
</div>

@endsection