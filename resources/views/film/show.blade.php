@extends('layout.master')

@section('judul')
    Detail Film {{$film->judul}}
@endsection

@section('content')

<img src="{{asset('img/' . $film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<p>Ringkasan: {{$film->ringkasan}}</p>
<p>Tahun: {{$film->tahun}}</p>
<p>Genre: {{$film->genre->nama}}</p>

@php($cast = \App\Cast::all())

<h3>Daftar Peran</h3>

@foreach ($film->peran as $item)
    <div class="card">
        <div class="card-body">
            <h5><b>{{$item->nama}}</b></h5>
            <p class="card-text">Pemain: {{$item->cast->nama}}<br>        
            Umur: {{$item->cast->umur}}</p>
        </div>
    </div>
@endforeach

<form action="/peran" method="POST" enctype="multipart/form-data" class=my-3>
    @csrf

    <div class="form-group">
        <label>Tambahkan Peran</label>
        <input type="hidden" name="film_id" value={{$film->id}} id="">
        <input name="nama" type="text" class="form-control" placeholder="Nama Peran">
        <select name="cast_id" class="form-control">
            <option value="">-- Pilih Pemeran --</option>
            @foreach ($cast as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
      </div>

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<a href="/film" class="btn btn-secondary">Kembali</a>
@endsection