@extends('layout.master')

@section('judul')
    Edit Film
@endsection

@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Film</label>
      <input type="text" name="judul" value="{{$film->judul}}" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Ringkasan</label>
      <textarea name="ringkasan" class="form-control">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Tahun</label>
      <input type="number" name="tahun" value="{{$film->tahun}}" class="form-control">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Poster</label>
      <input type="file" name="poster" class="form-control">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="form-control">
        <option value="">-- Pilih Genre --</option>
        @foreach ($genre as $genreitem)
            @if ($genreitem->id === $film->genre_id)
                <option value="{{$genreitem->id}}" selected>{{$genreitem->nama}}</option>
            @else
                <option value="{{$genreitem->id}}">{{$genreitem->nama}}</option>
            @endif
        @endforeach
      </select>
    </div>

    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
  
@endsection