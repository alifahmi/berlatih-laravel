@extends('layout.master')

@section('judul')
    Register
@endsection

@section('content')
    <h3>Buat Account Baru</h3>

    <h4>Sign Up Form</h4>

    <form action="/welcome" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" name="firstname">

        <p>Last name:</p>
        <input type="text" name="lastname">

        <p>Gender</p>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female</label><br>

        <p>Nationality</p>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="english">English</option>
            <option value="us">U.S.</option>
        </select>

        <p>Language Spoken</p>
        <input type="checkbox" name="bhsindo">Bahasa Indonesia<br>
        <input type="checkbox" name="english">English<br>
        <input type="checkbox" name="other">Other<br>

        <p>Bio</p>
        <textarea name="bio" rows="10" cols="30"></textarea><br>

        <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection