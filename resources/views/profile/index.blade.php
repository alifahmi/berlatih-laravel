@extends('layout.master')

@section('judul')
    Halaman Update Profile
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/u10xbgftgiw6lcaak096p8obsz15qhs4uq1vpkkm2ohq0qao/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama User</label>
        <input type="text" name="nama" value="{{$profile->user->name}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Email User</label>
        <input type="text" name="email" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>
   
    <div class="form-group">
        <label>Umur Profil</label>
        <input type="text" name="umur" value="{{$profile->umur}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Biodata Profil</label>
        <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat Profil</label>
        <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
        </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection