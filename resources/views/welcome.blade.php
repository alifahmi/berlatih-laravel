@extends('layout.master')

@section('judul')
    Welcome
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$namadepan}} {{$namablkg}}</h1>
    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
@endsection