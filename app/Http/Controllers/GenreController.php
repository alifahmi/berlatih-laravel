<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use RealRashid\SweetAlert\Facades\Alert;


class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
        ]);

        $genre = new Genre;
 
        $genre->nama = $request->nama;

        $genre->save();

        Alert::success('Tambah Genre', 'Genre Berhasil Ditambah');

        return redirect('/genre');
        
    }

    public function index()
    {
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    public function show($id)
    {
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }

    public function edit($genre_id)
    {
        $genre = Genre::where('id', $genre_id)->first();
        return view('genre.edit', compact('genre'));
    }

    public function update(Request $request, $genre_id)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
        ]);

        $genre = Genre::find($genre_id);
 
        $genre->nama = $request['nama'];
        
        $genre->save();
        Alert::success('Edit Genre', 'Genre Berhasil Diedit');
        return redirect('/genre');
    }

    public function destroy($genre_id)
    {
        $genre = Genre::find($genre_id);
 
        $genre->delete();
        Alert::success('Hapus Genre', 'Genre Berhasil Dihapus');
        return redirect('/genre');
    }
}
