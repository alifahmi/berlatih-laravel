<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function signup(Request $request) {
        // dd($request->all());
        $namadepan = $request['firstname'];
        $namablkg = $request['lastname']; 
        return view('welcome', compact('namadepan', 'namablkg'));
    }
}
