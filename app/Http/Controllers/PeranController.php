<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peran;
// Use Illuminate\Support\Facades\Auth;

class PeranController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $peran = new Peran;

        $peran->film_id = $request->film_id;
        $peran->cast_id = $request->cast_id;
        $peran->nama = $request->nama;
        
        $peran->save();
        return redirect()->back();
    }


}
